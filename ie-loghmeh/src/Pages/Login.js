import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import LOGO from "../Assets/LOGO.png";
import cc from "../Assets/my-icons-collection/copyright.png";
import "../style/login-register.css"
import PropTypes from 'prop-types';
import GoogleBtn from '../Components/auth/GoogleBtn';
import {withRouter} from 'react-router-dom';

//redux
import { connect } from 'react-redux';
import { loginUser } from '../actions/authActions'

class Login extends Component {
	constructor() {
		super();
		this.state = {
			email: '',
			password: '',
			errors: {},
		}
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}
	componentDidMount() {
		console.log(this.props.auth)
	  if (this.props.auth.isAuthenticated) {
	    this.props.history.push('/');
	  }
	}
	componentWillReceiveProps(nextProps) {
	  if (nextProps.auth.isAuthenticated) {
	    this.props.history.push('/dashboard');
	  }
	  if (nextProps.errors) {
			if(this.props.errors.error) {
				alert(this.props.errors.errors.login)
			}
	  }
	}
	onChange = e => this.setState({ [e.target.name]: e.target.value });
	onSubmit = e => {
	e.preventDefault();
		const userData = {
			email: this.state.email,
			password: this.state.password,
		}
		this.props.loginUser(userData, this.props.history);
	};
	render() {
	return (
		<div className="bodyy font1">
		<div className="bgg">
			<div className="img-c2 mt-6"><img src={LOGO} width="100" height="100" alt="" /></div>
			<h1 className="login">ورود</h1>
			<div className="form-wrapper2">

			<form onSubmit={this.onSubmit} method="post" id="myform">
				<div>
				<input 
					type="email"
					name="email"
					className= {'form-control mb-3'}
					value={this.state.email}
					onChange={this.onChange}
					placeholder="ایمیل"
				/>
				</div>
				<div>
				<input 
					type="password"
					name="password"
					className= {'form-control mb-3'}
					value={this.state.password}
					onChange={this.onChange}
					placeholder="رمز عبور"
				/>
				</div>
				<div className="mml mt-4">
				<input type="submit" name="register" className="btn btn-credit mt-3 " value="ورود" />
				</div>
			</form>

				<div className="mml">
					<Link type="submit" to='/register' className="btn btn-credit mt-3">ثبت نام</Link>
				</div>
				<div className='mml mt-3'>
					<GoogleBtn/>
				</div>
			</div>
		</div>
		<footer className="footer2">
			<img src={cc} width="20" height="20" alt="" />
			<p className="d-inline-block p-0 m-0 mr-2">تمامی حقوق متعلق به لقمه است.</p>
		</footer>

		</div>
	)
	}
}

Login.propTypes = {
loginUser: PropTypes.func.isRequired,
auth: PropTypes.object.isRequired,
errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
auth: state.auth,
errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(withRouter(Login));