import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Navbar from '../Components/layout/Navbar';
import Header from '../Components/Profile/Header';
import Charge from '../Components/Profile/Charge';
import Orders from '../Components/Profile/Orders';

import Footer from "../Components/layout/Footer"

class User extends Component {
  constructor() {
    super();
    this.state = {
      flag: true
    }
    this.onClick = this.onhandle.bind(this);
  }
  onhandle = e => this.setState({flag: !this.state.flag});
  render() {
    return (
      <div className='font1'>
        <Navbar />
        <Header />
        {this.state.flag ? <Orders click={this.onhandle} /> : <Charge click={this.onhandle} />}
        <Footer />
      </div>
    )
  }
}

User.propTypes = {
  flag: PropTypes.bool,
  // addCredit: PropTypes.func.isRequired,
  // loadOrders: PropTypes.func.isRequired
}

export default User;