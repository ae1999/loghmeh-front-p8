import React, { Component } from 'react';
import PropTypes from 'prop-types';

import starr from '../../Assets/my-icons-collection/star.png';

import { connect } from 'react-redux';
import {addToCartFP} from '../../actions/userActions';

class FoodPartyCart extends Component {
  onClickAddToCart = e => {
    this.props.addToCartFP({
      foodName: this.props.name,
      restaurantId: this.props.res_id,
    })
    window.location.reload(false);////////
  }
  render() {
    return (
      <div className="inside-scroll backgroundc mb-2 mx-3 restaurant-cart shadow-sm p-2 mt-3 bg-white">
        <div className="mx-2">

        <div className="row m-0">
          <div className="col-5 img-wrapper">
            <img className="rounded" src={this.props.img} width="85" height="85" alt="" />
          </div>
          <div className="col-7 justify-content-center pt-2">
              <span className="title-text float-right text-right w-100 title-text">{this.props.name}</span>
              <br/>
              <p className="d-inline mr-2 float-right">{parseInt(this.props.star*5)}</p>
              <img src={starr} width="15" height="15" alt=""  className="float-right d-inline" />
          </div>
        </div>
          <div className="mt-3">
            <p className="mb-3 d-inline-block mx-3 old-price-decoration">{this.props.oldPrice}</p>
            <p className="mb-3 d-inline-block">{this.props.price}</p>
          </div>

          <div className="">
            {this.props.count ? 
            (<p className="mb-3 count-style d-inline-block mx-3" dir='ltr'>{this.props.count}: موجودی</p>) :
            (<p className="mb-3 count-style d-inline-block mx-3" dir='ltr'> نا موجود</p>)
            }
            <button onClick={this.onClickAddToCart} className={"mb-3 d-inline-block" + this.props.count ? "buy-style" : "cant-buy"}>خرید</button>
          </div>

            <div className="res-info px-2 rounded pt-2">
              <p classNmae=''>{this.props.res}</p>
            </div>
        </div>
      </div>
    )
  }
}


FoodPartyCart.propTypes = {
  addToCartFP: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
  user: state.user,
});

export default connect(mapStateToProps, { addToCartFP })(FoodPartyCart);