import React, { Component } from 'react'
import CenterTitle from "../layout/CenterTitle";
import RestaurantCard from "./RestaurantCard";


export default class Restaurants extends Component {
	render() {
	let all;
	if(this.props.restaurants !== undefined || this.props.restaurants !== null) {
		all = this.props.restaurants.slice(0, 8).map((restaurant, i) =>
		<RestaurantCard restaurant={restaurant} key={"R"+i}/> );
	}
	return (
		<div className="restaurants-container mt-5">
			<CenterTitle text="رستوران ها"/>

			<div className="row justify-content-center mt-3">
				{all}
			</div>

		</div>
    )
  }
}
