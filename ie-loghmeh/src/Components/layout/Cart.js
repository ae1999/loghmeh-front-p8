import React from "react";
import {enToFaNumber} from "../../utils/utils";
import { getUserCart, finalizeOrder } from '../../actions/userActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BasketCart from '../Restaurant/BasketCart'
import isEmpty from '../../utils/validation/is-empty'

class Cart extends React.Component {
    constructor() {
        super();
        this.onClickFinalize = this.onClickFinalize.bind(this);
    }

    onClickFinalize() {
        this.props.finalizeOrder();
        window.location.reload(false);
    }
    render() {
            if(this.props.user.cart) {
                if(isEmpty(this.props.user.cart)) {
                    return (
                        <div className='my-2 mx-3'>buy some fooooooood</div>
                    );
                }
            }
            let cartItem =[]
            const {cart} = this.props.user;
            if(cart !== null || cart !== undefined) {
            if(cart.items !== undefined) {
                cartItem = cart.items.map((m, i) =>
                <BasketCart key={i} name={m.foodName} price={m.price} number={m.num} add={this.onClickadd} minus={this.onClickminus} />);
            }
            }
            return(
                <div className="restaurant-cart shadow p-3 mt-5 bg-white rounded">
                    <p className="border-bottom d-inline-block">سبد خرید</p>
                    <div className="cart-box p-3 px-5">
                    {cartItem}
                    </div>
                    <p className="mt-2 mb-1">جمع کل: {enToFaNumber(this.props.user.cart.totalPrice)} تومان</p>
                    <button onClick={this.onClickFinalize} className="btn btn-b btn-align">تایید نهایی</button>
                </div>
            )
        
    }
}

Cart.propTypes = {
    finalizeOrder: PropTypes.func.isRequired,
    getUserCart: PropTypes.func.isRequired,
  }
  
  const mapStateToProps = state => ({
    user: state.user
  });
  
  export default connect(mapStateToProps, {getUserCart, finalizeOrder})(Cart);
