import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { connect } from 'react-redux';
import {addCredit} from '../../actions/userActions';


class Charge extends Component {
  constructor() {
    super();
    this.state = {
      credir: ''
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange = e => this.setState({ [e.target.name]: e.target.value });
  onSubmit = e => {
    e.preventDefault();
    const data = {
      credit: this.state.credit,
    }
    this.props.addCredit(data)
  };
    
  render() {
    return (
      <div className='aa'>
      <div className=" charge-box">
        <div className="btn-group btns" dir="ltr">
          <button className="btn lb">افزایش اعتبار</button>
          <button onClick={this.props.click} className="btn rb">سفارش ها</button>
        </div>
        <div>
          <form onSubmit={this.onSubmit} className="charge-wrapper">
            <input type="number" name='credit' value={this.state.name} onChange={this.onChange} className="form-control input-credit" id="inputPassword2" placeholder="میزان افزایش اعتبار" />
            <button type="submit" className="btn btn-credit">افزایش</button>
          </form>
        </div>
      </div>
      </div>
    )
  }
}

Charge.propTypes = {
  addCredit: PropTypes.func.isRequired,
  credit: PropTypes.string
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
  user: state.user,
});

export default connect(mapStateToProps, { addCredit })(Charge);