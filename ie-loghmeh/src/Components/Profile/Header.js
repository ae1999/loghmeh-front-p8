import React, { Component } from 'react';
import PropTypes from 'prop-types';

import UserPicture from '../../Assets/my-icons-collection/account.png';

import { connect } from 'react-redux';
import {getUserInfo} from '../../actions/userActions';

class Header extends Component {
  componentDidMount() {
    this.props.getUserInfo();
  }
  render() {
    const {info} = this.props.user
    return (
      <div className="my-container info-sec">
        <div className="row">
          <div className="col-8">
            <div className="list-items pt-3 mr-4">
              <img src={UserPicture} className="ml-3" alt="" width="60" height="60" />
              <h2 className="d-inline" dir="ltr">{info.name}</h2>
            </div>
          </div>
          <div className="col-4">
            <ul>
              <li className="list-items">
                <i className="flaticon-phone"></i>
                <p className="d-inline" dir="ltr">{info.phone}</p>
              </li>
              <li className="list-items">
                <i className="flaticon-mail"></i>
                <p className="d-inline" dir="ltr">{info.email}</p>
              </li>
              <li className="list-items">
                <i className="flaticon-card cart-style"></i>
                <p className="d-inline" dir="ltr">{info.credit}</p>
                <p className="d-inline" dir="ltr">تومان</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    
    )
  }
}

Header.propTypes = {
  getUserInfo: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
  user: state.user,
});

export default connect(mapStateToProps, { getUserInfo })(Header);