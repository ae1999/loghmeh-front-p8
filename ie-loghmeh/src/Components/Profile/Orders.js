import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import {loadOrders} from '../../actions/userActions';

import Modal from "react-bootstrap/Modal";
import OrderDetail from "./OrderDetail";

import {enToFaNumber} from "../../utils/data/res";

class Orders extends Component {
  constructor() {
    super();
    this.state = {
      show: false
    };
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  handleClose() {
    this.setState({ show: false });
  }
  handleShow() {
    this.setState({show: true});
  }
  componentDidMount() {
    this.props.loadOrders();
  }
  render() {
    let data = []
    const {orders} = this.props.user
    console.log(orders);
    if(orders !== undefined || orders !== null) {
      data = orders.map((m,i) =>
         (
          <div key={i}>
            <div className="pb-2 px-4">
              <div className="row w-80 mx-auto py-1 mb-4 vivaldi border py-2">
              <div className="col-6 col-md-1 border-left">{enToFaNumber(orders.length-i)}</div>
                <div className="col-6 col-md-7 border-left">{m.restaurantName}</div>
                {
                  m.orderStatus === "InSearchForDelivery" 
                  ? 
                  <div value={i} onClick={this.handleShow} className="col-6 col-md-4"><div className="search px-2 rounded d-inline-block">در جست و جوی پیک</div></div> 
                  : 
                  ( 
                    m.orderStatus === "Delivered" 
                    ? 
                    <div value={i} onClick={this.handleShow} className="col-6 col-md-4"><div className="bill px-2 rounded d-inline-block">مشاهده ی فاکتور</div></div> 
                    : 
                    <div value={i} onClick={this.handleShow} className="col-6 col-md-4"><div className="in-way px-2 rounded d-inline-block">پیک در مسیر</div></div> 
                  )
                }
              </div>
            </div>
            <Modal size="lg" show={this.state.show} onHide={this.handleClose}>
              <OrderDetail cart={orders[i]}/>
            </Modal>
          </div>
        )
      )
    }
    data.reverse()
    return (
      <div>
        <div className="charge-box">
          <div className="btn-group btns" dir="ltr">
            <button onClick={this.props.click} className="btn lb2">افزایش اعتبار</button>
            <button className="btn rb2">سفارش ها</button>
          </div>
          {data}
        </div>
      </div>
    )
  }
}

Orders.propTypes = {
  loadOrders: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
  user: state.user,
});

export default connect(mapStateToProps, { loadOrders })(Orders);
