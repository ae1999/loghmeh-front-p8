import axios from "axios";

var axiosInstance = axios.create({
  baseURL: "http://ie.etuts.ir:31009",
  responseType: "json",
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    Accept: "application/json"
  }
});

// axiosInstance.interceptors.request.use(config => {
//   let userInfo = JSON.parse(localStorage.getItem("USER_INFO"));
//   config.headers.Authorization = userInfo ? `bearer ${userInfo.token}` : "";
//   return config;
// });

axiosInstance.interceptors.response.use( response => response );

export default axiosInstance;
