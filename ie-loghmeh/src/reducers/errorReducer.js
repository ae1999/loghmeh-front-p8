import { GET_ERRORS } from '../utils/actionTypes';
import isEmpty from '../utils/validation/is-empty';

const initialState = {
  error: false,
  errors: {}
};

export default function(state = initialState, action) {
  switch(action.type) {
    case GET_ERRORS: {
      console.log(action.payload)
      return {
        ...state,
        error: !isEmpty(action.payload),
        errors: action.payload
      };
    }
    default:
      return state;
  }
}