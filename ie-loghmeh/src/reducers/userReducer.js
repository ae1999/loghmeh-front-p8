import { GET_USER_INFO, 
        GET_USER_CART,
        LOAD_ORDERS,
        FINALIZE_ORDER } from '../utils/actionTypes';

const initialState = {
  cart: {},
  info: {},
  orders: []
}

export default function(state = initialState, action) {
  switch(action.type) {
    case GET_USER_CART:
      return {
        ...state,
        cart: action.payload
      };
    case FINALIZE_ORDER:
      return {
        ...state,
        cart: action.payload
      };
    case GET_USER_INFO:
      return {
        ...state,
        info: action.payload
      };
    case LOAD_ORDERS:
      return {
        ...state,
        orders: action.payload
      };
    default:
      return state;
  }
}