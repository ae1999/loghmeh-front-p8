import axios from '../utils/request';
import { GET_USER_INFO, 
        GET_USER_CART,
        LOAD_ORDERS,
        ADD_TO_CART,
        GET_ERRORS } from '../utils/actionTypes';
import {user} from "../utils/data/res";

export const getUserCart = () => dispatch => {
  console.log('getting user cart');
  axios
    .get('/users/cart')
    .then(res =>
      dispatch({
        type: GET_USER_CART,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"userCart": "server error loading user cart"}
      })
    );
};

export const finalizeOrder = () => dispatch => {
  console.log('finalizing...');
  axios
    .get('/users/finalize_order')
    .then(res =>
      console.log('finalized')
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"fanilize": "server error finalizing cart"}
      })
    );
};

export const addToCart = data => dispatch => {
  console.log('adding to cart', data);
  axios
    .post('/users/add_to_cart', data)
    .then(res =>
      dispatch({
        type: ADD_TO_CART,
        payload: {} 
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"fanilize": "server error addign to cart"}
      })
    );
};

export const addToCartFP = data => dispatch => {
  axios
    .post('/food_party/add_to_cart', data)
    .then(res =>
      dispatch({
        type: ADD_TO_CART,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"addToCartFP": "server error addign to cart"}
      })
    );
};

export const addCredit = data => dispatch => {
  console.log('adding credit ', data);
  axios
    .post('/users/add_credit', data)
    .then(res =>
      console.log(res)
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"addCredit": "server error addign credit"}
      })
    );
};

export const getUserInfo = () => dispatch => {
  console.log('getting user info ...');
  axios
    .get('/user')
    .then(res =>
      dispatch({
        type: GET_USER_INFO,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"user info": "server error geting user info"}
      })
    );
  dispatch({
    type: GET_USER_INFO,
    payload: user
  })
};

export const loadOrders = () => dispatch => {
  console.log('loading orders ...');
  axios
    .get('/users/orders')
    .then(res =>
      dispatch({
        type: LOAD_ORDERS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"user info": "hi server error geting user info"}
      })
    );
};
